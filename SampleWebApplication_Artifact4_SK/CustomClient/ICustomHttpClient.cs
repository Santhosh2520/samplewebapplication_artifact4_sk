namespace SampleWebApplication_Artifact4_SK.CustomClient
{
    public interface ICustomHttpClient
    {
        Task<HttpResponseMessage> GetAsync(string url);
        Task<HttpResponseMessage> PostJsonAsync<T>(string url, T requestBody);
    }
}