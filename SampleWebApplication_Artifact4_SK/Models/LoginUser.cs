namespace SampleWebApplication_Artifact4_SK.Models
{
    public class LoginUserRequest
    {
        public String email { get; set; }

        public String password { get; set; }
    }

   
    public class LoginUserResponse
    {
        public String token { get; set; }

    }
}
