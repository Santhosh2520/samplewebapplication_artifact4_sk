using SampleWebApplication_Artifact4_SK.Models;

namespace SampleWebApplication_Artifact4_SK.Services
{
    public interface IUserService
    {
        Task<UserDetails> GetUserDetails(int id);
        Task<CreateUserResponse> CreateUser(CreateUserRequest createUserRequest);

        Task<LoginUserResponse> LoginUser(LoginUserRequest loginUserRequest);
    }
}
